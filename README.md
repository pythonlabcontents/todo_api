# todo_api
Python学習コンテンツ用です。

# Markdown表記
このドキュメントはMarkdownで記述しています。
VSCode上でプレビューを見る場合は、VSCode上で下記のショートカットコマンド実行
```
Ctrl + Shift + K ⇒ Ctrl + Shift + V
```

# 環境構築方法
1. ソースコードを取得し、VSCodeのワークスペースにtodo_apiフォルダを追加

2. Python仮想環境作成  
※Python仮想環境を作ることで、同じ開発マシン内の別のPythonプロジェクトとは独立させてパッケージ管理を行うことが可能  
VSCodeのターミナルでtodo_apiディレクトリを開き、下記のコマンドを実行
```
python -m venv 環境名
※環境名は例として「env_api」 など
```

3. Python仮想環境をactivate  
下記のコマンドを実行  
実行後、コマンドラインの先頭に緑色で（env_api）とつくことを確認  
※今後開発をする際にはこちらのコマンドを実行して仮想環境をactivateする必要があるので注意  
```
./env_api/Scripts/activate
```

4. 必要なパッケージをインストール
```
pip install -r requirements.txt
※requirements.txt内に記述されているパッケージ一覧をまとめてインストールするコマンド
```

5. パッケージがインストールされていることを確認  
下記コマンドの実行結果が、requirements.txtと一致していることを確認する
```
pip freeze
```


# 学習利用方法
* app/app.pyのソースコードの完成を目指してください
  * app/app.pyには"＜＞"のついたコメント文を埋め込んでいます
    このコメントの入っている箇所すべてに正しいコードを記述することで、プログラムが完成します
  * 実践でも使われるWebフレームワークやDB等のライブラリの使い方に関する穴埋めが多いため、実際に使い方を調べながら埋めていきましょう

* 模範解答用のスクリプトは「answer」ブランチにあります。
  VSCode上でGitブランチの切り替えを行うか、GitLabのページよりご確認ください。

* もし完成した場合は、新しい機能を追加する等自由にカスタマイズすることで、Pythonのコーディングに慣れていきましょう
  * カスタマイズ例
    * 全件削除するAPIの新規実装
    * バリデーション機能の強化


# Webサーバ起動
* 下記コマンドでFastAPIを用いたWebサーバを起動できる
```
uvicorn app.app:app --reload
※app.app:appとは、appディレクトリのapp.pyの中にあるappというインスタンスを起動するという意味
```

* 下記のURLへアクセスする(FastAPIのSwagger画面)
```
http://127.0.0.1:8000/docs
※APIサーバとしてのエンドポイントURlはhttp://127.0.0.1となる
```


# FastAPIで自動生成されるドキュメント
Webサーバ起動時に、URlの最後にdocs or redocを付与すると、FastAPIにて自動生成されたドキュメントにアクセスできる
## Swagger UI
```
http://127.0.0.1:8000/docs
```
* Swagger UI画面上から各APIにリクエストをGUIで送ることができる。簡単な動作確認などで便利な機能
* 下記の手順でリクエストを送ることができる
1. リンクを開く
1. リクエストを送りたいAPIを開く
1. 開いた中にある「Try it out」を押す
1. リクエストに必要なパラメーターを設定したうえで「Execute」を押す
1. Responseを確認する

## API仕様書
```
http://127.0.0.1:8000/redoc
```
* APIの仕様書を開発者以外へ配布する際に便利な機能


# flake8によるコードの静的解析
* 下記コマンドでソースコードがPEP8（Python標準のコーディング規約）に準拠しているかをチェックできる
```
flake8 <ディレクトリ or ファイルパス>
※ディレクトリを指定した場合は、ディレクトリ配下にあるPythonファイルをすべてチェックする
```
* 複数人で開発をする場合、それぞれが独自の書き方をすると可読性の低下につながるため、コーディング規約に則った形で開発する癖をつけておくのがよい。


# 参考リンク
* FastAPI  
https://fastapi.tiangolo.com/ja/

* SQLite3（株式会社システムインテグレータ「PythonからSQLiteを操作する方法」より）  
https://products.sint.co.jp/topsic/blog/how-to-use-sqlite-in-python
