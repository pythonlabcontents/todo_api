# ＜ここにFastAPI import処理を追加＞
from pydantic import BaseModel, Field
from datetime import date
import uuid
import sqlite3


# FastAPIインスタンスを作成
# ＜ここに処理を追加＞

# SQLiteはdbファイル内で疑似的なDBを管理するライブラリ
# dbファイル名を定義
dbname = 'todo.db'

# SQLiteとのコネクションを作成
# ＜コネクションを生成しconn変数に格納する処理を追加＞

# SQLiteを操作するためのカーソルを作成
# ＜カーソルを生成しcur変数に格納する処理を追加＞

# テーブル削除したい場合はこちら
# cur.execute("DROP TABLE todolist")

# テーブルの作成(テーブルが存在しない場合のみ実行される)
cur.execute(
    """
    CREATE TABLE IF NOT EXISTS todolist
    (id STRING PRIMARY KEY, title STRING, done INTEGER, deadline STRING)
    """
)

# カーソルのクローズ
# ＜ここに処理を追加＞
# コネクションのクローズ
# ＜ここに処理を追加＞


class Todo(BaseModel):
    """
    ToDoタスクのクラス

    title: タスクのタイトル
    done: タスクの完了状況(0: 未完了 / 1: 完了)
    deadline: タスクの対応期日(yyyy-mm-dd形式)
    """
    # ＜ここにtitle, done, deadlineそれぞれの定義と型を1行ずつ、合計3行追加＞
    # doneは0と1以外の数字が入ってこないように、Fieldのgeとleを用いて範囲指定する
    # deadlineはdate型を指定する


@app.get('/')
def root():
    """
    サンプル用API
    """
    return {'Hello': "World!"}


@app.get('/all')
def getAllTodo() -> dict:
    """
    TodoリストをDict形式のListで返却するAPIです
    """
    # SQLiteとのコネクションを作成
    # ＜ここに処理を追加＞
    # SQLiteを操作するためのカーソルを作成
    # ＜ここに処理を追加＞
    todo_list = []
    # ＜todolistテーブルから全件取得するSQL処理を追加(他の関数を参考に)＞
    # tupleをdict化したうえで、todo_listに1つ1つ追加する
    for x in cur.fetchall():
        todo_list.append(tuple_to_dict(x))
    # カーソルのクローズ
    # ＜ここに処理を追加＞
    # コネクションのクローズ
    # ＜ここに処理を追加＞

    # json形式で返却する
    return {'todolist': todo_list}


@app.get('/todo')
def getTodo(id: str) -> dict:
    """
    Todoリストのタスクを1件取得するAPIです

    - **id**: タスクのID
    """
    # SQLiteとのコネクションを作成
    # ＜ここに処理を追加＞
    # SQLiteを操作するためのカーソルを作成
    # ＜ここに処理を追加＞
    cur.execute("SELECT * FROM todolist WHERE ID = ?", (id,))
    todo = cur.fetchall()
    # カーソルのクローズ
    # ＜ここに処理を追加＞
    # コネクションのクローズ
    # ＜ここに処理を追加＞

    # json形式で返却する
    return tuple_to_dict(todo[0])


# ＜/todoに対してpostメソッドを受け取れるようにためにデコレータを設定＞
def createTodo(todo: Todo):
    """
    Todoリストにタスクを追加するAPIです

    - **title**: タスクのタイトル
    - **done**: タスクの完了状況(0: 未完了 / 1: 完了)
    - **deadline**: タスクの対応期日(yyyy-mm-dd形式)
    """
    # SQLiteとのコネクションを作成
    # ＜ここに処理を追加＞
    # SQLiteを操作するためのカーソルを作成
    # ＜ここに処理を追加＞
    # Todoごとに一意なuuidを生成
    # ＜uuidライブラリのuuid4()メソッドを用いて取得した結果をstr型に変換してid変数に格納する処理＞
    cur.execute("""
                  INSERT INTO todolist VALUES (?, ?, ?, ?)
                """,
                (id, todo.title, todo.done, str(todo.deadline))
                )
    conn.commit()
    # カーソルのクローズ
    # ＜ここに処理を追加＞
    # コネクションのクローズ
    # ＜ここに処理を追加＞
    return


@app.put('/todo/{id}')
def updateTodo(id: str, todo: Todo):
    """
    Todoリストのタスクを更新するAPIです

    - **id**: タスクのID
    - **title**: タスクのタイトル
    - **done**: タスクの完了状況(0: 未完了 / 1: 完了)
    - **deadline**: タスクの対応期日(yyyy-mm-dd形式)
    """
    # SQLiteとのコネクションを作成
    # ＜ここに処理を追加＞
    # SQLiteを操作するためのカーソルを作成
    # ＜ここに処理を追加＞
    cur.execute("""
                    UPDATE todolist
                    SET title=?, done=?, deadline=? where id=?
                """,
                # ＜ここにプレースホルダを用いた値設定処理を追加(他の関数を参考)＞
                )
    conn.commit()
    # カーソルのクローズ
    # ＜ここに処理を追加＞
    # コネクションのクローズ
    # ＜ここに処理を追加＞
    return


# ＜/todoに対してdeleteメソッドを受け取れるようにためにデコレータを設定。またパスパラメータでidを受け取る＞
def deleteTodo(id: str):
    """
    Todoリストのタスクを削除するAPIです

    - **id**: タスクのID
    """
    # SQLiteとのコネクションを作成
    # ＜ここに処理を追加＞
    # SQLiteを操作するためのカーソルを作成
    # ＜ここに処理を追加＞
    cur.execute("DELETE FROM todolist WHERE ID = ?", (id,))
    conn.commit()
    # カーソルのクローズ
    # ＜ここに処理を追加＞
    # コネクションのクローズ
    # ＜ここに処理を追加＞
    return


def tuple_to_dict(todo_tuple: tuple) -> dict:
    """
    DBから取得したTuple形式の各レコードをdictに変換する関数

    - **todo_tuple**: 各レコードのTuple(DBからSQLで取得した結果)
    """

    columns = ('id', 'title', 'done', 'deadline')

    # {'id': id, 'title': title, 'done': done, 'deadline': deadline'}形式のdictを生成
    todo_dict = {key: value for key, value in zip(columns, todo_tuple)}
    return todo_dict
